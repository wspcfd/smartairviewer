const fullScreenRenderer = vtk.Rendering.Misc.vtkFullScreenRenderWindow.newInstance({background:[0,0,0,0]});
const vtkColorMaps = vtk.Rendering.Core.vtkColorTransferFunction.vtkColorMaps;

const sliceurls = ['https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/ACH.vtp',
                   'https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/FAR.vtp',
                   'https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/vel.vtp',
                   'https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/fACH.vtp',
                   'https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/CO2.vtp',
                ];

const stlurls = [{url: 'https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/walls.stl', opacity: "1.0"},
                {url: 'https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/furniture.stl', opacity: "1.0"}
                ];

const streamurls = ['https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/streams.vtp',
                   ];

const edgeurls = ['https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/walls_edges.vtp', 
                'https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/furniture_edges.vtp'
                ];
const licurls = ['https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/licglyph.vtp'
                ];

var renderer = fullScreenRenderer.getRenderer();
var renderWindow = fullScreenRenderer.getRenderWindow();
renderer.getActiveCamera().setPosition(-10.460718403281259, -41.079852143396536, 45.05497774499336);
renderer.getActiveCamera().setFocalPoint(-10.458100089004823, -41.076304136976084, 45.052607153719684);
renderer.getActiveCamera().setViewUp(0.0, 0.0, 1.0);
renderer.setTwoSidedLighting(false);
renderer.setLightFollowCamera(false);
// renderer.removeLight(renderer.getLights());
renderer.removeAllLights();
var newlight1 = vtk.Rendering.Core.vtkLight.newInstance();
newlight1.setPosition(50, 50, 20);
newlight1.setFocalPoint(28, 20, 0);
var newlight2 = vtk.Rendering.Core.vtkLight.newInstance();
newlight2.setPosition(0, 0, 20);
newlight2.setFocalPoint(28, 20, 0);
var newlight3 = vtk.Rendering.Core.vtkLight.newInstance();
newlight3.setPosition(28, 20, 10);
newlight3.setFocalPoint(28, 20, 0);
// newlight3.setIntensity(0.4);
renderer.addLight(newlight1);
renderer.addLight(newlight2);
// renderer.addLight(newlight3);

function displayStl(file) {
    stlurl = file.url;
    var stlMapper = vtk.Rendering.Core.vtkMapper.newInstance({ 
        scalarVisibility: false,
    });
    var stlActor = vtk.Rendering.Core.vtkActor.newInstance();
    var stlReader = vtk.IO.Geometry.vtkSTLReader.newInstance();

    stlActor.getProperty().setOpacity( file.opacity );
    stlActor.setMapper(stlMapper);
    stlMapper.setInputConnection(stlReader.getOutputPort());
    stlActor.getProperty().setColor(1, 1, 1);

    stlReader.setUrl(stlurl).then(() => {
        renderer.addActor(stlActor);
        renderer.resetCamera();
        renderWindow.render();
    });

}

for (var i = stlurls.length -1; i >= 0; i--)
{
    displayStl(stlurls[i]);
}


function displayEdges(file) {
    edgeurl = file;
    var edgeMapper = vtk.Rendering.Core.vtkMapper.newInstance({
        scalarVisibility: false,
    });
    var edgeActor = vtk.Rendering.Core.vtkActor.newInstance();
    var edgeReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

    edgeActor.setMapper(edgeMapper);
    edgeMapper.setInputConnection(edgeReader.getOutputPort());
    edgeActor.getProperty().setColor(0, 0, 0);

    edgeReader.setUrl(edgeurl).then(() => {
        renderer.addActor(edgeActor);
        renderer.resetCamera();
        renderWindow.render();
    });
}

for (var i = edgeurls.length -1; i >= 0; i--)
{
    displayEdges(edgeurls[i]);
}

// console.log(vtk.Rendering.Core.vtkColorTransferFunction.vtkColorMaps.rgbPresetNames);
var lookupTable = vtk.Rendering.Core.vtkColorTransferFunction.newInstance();
var presetU = vtkColorMaps.getPresetByName('Rainbow Desaturated');
var presetFAR = vtkColorMaps.getPresetByName('Viridis (matplotlib)');
var presetCO2 = vtkColorMaps.getPresetByName('Cool to Warm');
var presetfACH = vtkColorMaps.getPresetByName('Black, Blue and White')
var table = presetFAR.RGBPoints;
var tableInvert = [];
for (var i = table.length/4-1; i >= 0;  i--) {
     tableInvert.push(-1*table[i*4], table[i*4+1], table[i*4+2], table[i*4+3]);
}
presetFAR.RGBPoints = tableInvert;
var table = presetfACH.RGBPoints;
var tableInvert = [];
for (var i = table.length/4-1; i >= 0;  i--) {
     tableInvert.push(-1*table[i*4], table[i*4+1], table[i*4+2], table[i*4+3]);
}
presetfACH.RGBPoints = tableInvert;

var vtkMapper = vtk.Rendering.Core.vtkMapper.newInstance({
        interpolateScalarsBeforeMapping: true,
        useLookupTableScalarRange: false,
        lookupTable,
        scalarVisibility: true,
    });

var vtkActor = vtk.Rendering.Core.vtkActor.newInstance();
var vtpReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

vtkMapper.setInputConnection(vtpReader.getOutputPort());
// vtkActor.setMapper(vtkMapper);


vtkMapper.setInputConnection(vtpReader.getOutputPort());
// vtkActor.setMapper(vtkMapper);

//for lic:
var vtkMapper2 = vtk.Rendering.Core.vtkMapper.newInstance({
            interpolateScalarsBeforeMapping: false,
            useLookupTableScalarRange: false,
            scalarVisibility: false,
        });
var vtkActor2 = vtk.Rendering.Core.vtkActor.newInstance();
var vtpReader2 = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();


fullScreenRenderer.addController('<table>\
        <tr>\
            <td><img width=240 \
            src="./smartair_logo.png"></td>\
            </tr>\
        <tr>\
        <tr>\
            <td><b>Property: </b>WSP Montreal</td>\
            </tr>\
        <tr>\
        <tr>\
            <td><b>Case: </b>Level 18</td>\
            </tr>\
        <tr>\
        <tr>\
            <td><b>Analysis: </b>Air changes</td>\
            </tr>\
        <tr>\
        <br>\
        <tr>\
            <td><img id="achlegend" width=340 \
            src="https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/legend.jpg"></td>\
            </tr>\
            <td><img id="vellegend" width=340 \
            src="https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/velocitylegend.JPG"></td>\
            </tr>\
            <td><img id="farlegend" width=340 \
            src="https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/farlegend.jpg"></td>\
            </tr>\
            <td><img id="fachlegend" width=340 \
            src="https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/fachlegend.JPG"></td>\
            </tr>\
            <td><img id="co2legend" width=340 \
            src="https://joshmillar.gitlab.io/vtks/static/45889637e465b51ee34113bb4188c5_property7/co2legend.JPG"></td>\
            </tr>\
        <tr>\
            <td><h6>Select visualisation:</h6></td>\
            </tr>\
        <tr>\
            <td>\
                <select class="arrays" style="width: 100%">\
                    <option value="0" selected>None</option>\
                    <option value="2">Fresh air changes hourly</option>\
                    <option value="3">Fresh air rate</option>\
                    <option value="4">Velocity</option>\
                    <option value="5">CO2</option>\
                </select>\
                </td>\
                </tr>\
                <tr>\
            <td>\
                <button id="planbutton">\
                Plan view\
                </button>\
                <button id="camerabutton">\
                Reset view\
                </button>\
                </td>\
            </tr>\
        </table>');

var arrayselector = document.querySelector(".arrays")
var arrayselected = 0;
var planViewButton = document.getElementById("planbutton")
var resetViewButton = document.getElementById("camerabutton")

function removelegends() {
    document.getElementById("achlegend").style.display = "none";
    document.getElementById("vellegend").style.display = "none";
    document.getElementById("farlegend").style.display = "none";
    document.getElementById("fachlegend").style.display = "none";
    document.getElementById("co2legend").style.display = "none";
}

arrayselector.addEventListener('change', (e) => {
    arrayselected = Number(e.target.value);
    if (arrayselected == 1){ //ACH
        vtpReader.setUrl(sliceurls[0]).then(() => {
        removelegends();
        renderer.removeActor(vtkActor2);
        lookupTable.removeAllPoints();
        lookupTable.addRGBPoint(2, 1.0, 1.0, 0.0);
        lookupTable.addRGBPoint(5, 0.0, 1.0, 0.0);
        lookupTable.addRGBPoint(8, 0.0, 0.7, 1.0);
        lookupTable.setAboveRangeColor(0.0, 0.0, 1.0, 1.0);
        lookupTable.setUseAboveRangeColor(true)
        lookupTable.setBelowRangeColor(1.0, 0.0, 0.0, 1.0);
        lookupTable.setUseBelowRangeColor(true)
        lookupTable.setDiscretize(true);
        lookupTable.setNumberOfValues(3);
        vtkMapper.setScalarModeToUsePointFieldData();
        vtkMapper.setScalarRange([2, 8]);
        vtkMapper.setColorByArrayName('ACH');
        vtkMapper.setScalarVisibility(true)
        vtkActor.setMapper(vtkMapper);
        renderer.removeActor(vtkActor2);
        renderer.addActor(vtkActor);
        document.getElementById("achlegend").style.display = "block";
        // renderer.resetCamera();
        renderWindow.render();
    })} else if (arrayselected == 2) { //fACH
        vtpReader.setUrl(sliceurls[3]).then(() => {
        removelegends();
        renderer.removeActor(vtkActor2);
        lookupTable.removeAllPoints();
        lookupTable.applyColorMap(presetfACH);
        lookupTable.setDiscretize(true);
        lookupTable.setNumberOfValues(10);
        vtkMapper.setScalarModeToUsePointFieldData();
        vtkMapper.setScalarRange([0, 2]);
        vtkMapper.setColorByArrayName('fACH');
        vtkMapper.setScalarVisibility(true)
        vtkActor.setMapper(vtkMapper);
        renderer.removeActor(vtkActor2);
        renderer.addActor(vtkActor);
        document.getElementById("fachlegend").style.display = "block";
        vtkActor.setMapper(vtkMapper);
        renderer.addActor(vtkActor);
        renderWindow.render();
    })} else if (arrayselected == 3) { //FAR
        vtpReader.setUrl(sliceurls[1]).then(() => {
        removelegends();
        renderer.removeActor(vtkActor2);
        lookupTable.setUseBelowRangeColor(false)
        lookupTable.setUseAboveRangeColor(false)
        lookupTable.removeAllPoints();
        lookupTable.applyColorMap(presetFAR);
        lookupTable.setDiscretize(true);
        lookupTable.setNumberOfValues(16);
        vtkMapper.setScalarModeToUsePointFieldData();
        vtkMapper.setScalarRange([0, 0.8]);
        vtkMapper.setColorByArrayName('FAR');
        document.getElementById("farlegend").style.display = "block";
        vtkActor.setMapper(vtkMapper);
        renderer.addActor(vtkActor);
        renderWindow.render();
    })} else if (arrayselected == 4){ //vel
        vtpReader.setUrl(sliceurls[2]).then(() => {
        removelegends();
        lookupTable.removeAllPoints();
        lookupTable.applyColorMap(presetU);
        lookupTable.setDiscretize(false);
        lookupTable.setUseBelowRangeColor(false)
        lookupTable.setUseAboveRangeColor(false)
        // lookupTable.setNumberOfValues(16);
        vtkMapper.setScalarModeToUsePointFieldData();
        vtkMapper.setScalarRange([0, 0.4]);
        vtkMapper.setColorByArrayName('magU');
        document.getElementById("vellegend").style.display = "block";
        vtkActor.setMapper(vtkMapper);
        renderer.addActor(vtkActor);
        renderWindow.render();
    })
        //add lics:
        vtkMapper2.setInputConnection(vtpReader2.getOutputPort());
        vtpReader2.setUrl(licurls[0]).then(() => {
        vtkActor2.getProperty().setColor(1, 1, 1);
        vtkActor2.getProperty().setOpacity(0.6);
        vtkActor2.setMapper(vtkMapper2);
        renderer.addActor(vtkActor2);
        renderWindow.render();
    })} else if (arrayselected == 5) { //CO2
        vtpReader.setUrl(sliceurls[4]).then(() => {
        removelegends();
        renderer.removeActor(vtkActor2);
        lookupTable.setUseBelowRangeColor(false)
        lookupTable.setUseAboveRangeColor(false)
        lookupTable.removeAllPoints();
        lookupTable.applyColorMap(presetCO2);
        lookupTable.setDiscretize(true);
        lookupTable.setNumberOfValues(16);
        vtkMapper.setScalarModeToUsePointFieldData();
        vtkMapper.setScalarRange([2500, 3500]);
        vtkMapper.setColorByArrayName('CO2');
        document.getElementById("co2legend").style.display = "block";
        vtkActor.setMapper(vtkMapper);
        renderer.addActor(vtkActor);
        renderWindow.render();
    })} else {
        renderer.removeActor(vtkActor);
        renderer.removeActor(vtkActor2);
        renderWindow.render(); 
    }

});

// reset camera:
resetViewButton.addEventListener('click', (e) => {
        renderer.getActiveCamera().setPosition(-10.460718403281259, -41.079852143396536, 45.05497774499336);
        renderer.getActiveCamera().setFocalPoint(-10.458100089004823, -41.076304136976084, 45.052607153719684);
        renderer.getActiveCamera().setViewUp(0.0, 0.0, 1.0);
        renderer.resetCamera();
        renderWindow.render();
    });
planViewButton.addEventListener('click', (e) => {
        renderer.getActiveCamera().setPosition(27.9, 19.55, 73.58);
        renderer.getActiveCamera().setFocalPoint(27.9, 19.55, 1.5);
        renderer.getActiveCamera().setViewUp(0.0, 1.0, 0.0);
        renderer.resetCamera();
        renderWindow.render();
    });